$(function () {

    /* Select optie detectie */
    $('input[type="radio"]').click(function(){
        var optionValue = $(this).attr("value");
        $(".doc-type").html(optionValue).css("text-transform", "capitalize"); // Verander tekst naar gekozen waarde
        $("#doc-filterer").focus();

        /* Haal JSON op */
        $.getJSON("assets/json/codator.json", function (result) {

            /* Initiele waarde aantal documenten */
            let docCount = 0;

            /* Tabel leegmaken alvorens nieuwe rijen worden opgehaald */
            $("#docs").html("");

            /* Voor ieder regel */
            $.each(result, function (i, field) {

                let docLabel    = field.dossier; // Naam document
                let docStatus   = field.status;  // Status document
                let docMediator = field.mediator; // Emailadres Mediator
                // Aantal opgehaalde documenten

                /* Haal alleen de rijen op die overeenkomen met gekozen optie */
                if (docStatus === optionValue || optionValue === "Alle") {

                    docCount += 1; // tel aantal opgehaalde rijen die overeenkomen met gekozen optie

                    /* Maak tabel rijen aan */
                    let tableRow = "<tr class='appended-row'><td>" + docLabel + "</td><td>" + docMediator + "</td><td>4 dagen, 23 uur geleden</td><td><a href='#!'>vragenlijst</a></td><td>"+docStatus+"</td><tr>";

                    /* Zet opgehaalde rijen in de tabel */
                    $("#docs").append(tableRow);
                    $(".doc-count").html(docCount);
                }

            });
        });
    });


    
    /* Keyword zoekfunctie */
    $('#doc-filterer').keyup(function() {

        let $rows = $('#docs-table .appended-row');

        let val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
        
        $rows.show().filter(function() {
            let text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
            return !~text.indexOf(val);
        }).hide();

    });


})