<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/foundation-sites@6.5.3/dist/css/foundation.min.css" integrity="sha256-xpOKVlYXzQ3P03j397+jWFZLMBXLES3IiryeClgU5og= sha384-gP4DhqyoT9b1vaikoHi9XQ8If7UNLO73JFOOlQV1RATrA7D0O7TjJZifac6NwPps sha512-AKwIib1E+xDeXe0tCgbc9uSvPwVYl6Awj7xl0FoaPFostZHOuDQ1abnDNCYtxL/HWEnVOMrFyf91TDgLPi9pNg==" crossorigin="anonymous">
        <link rel='stylesheet' href="assets/css/style.css" >
        
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Pacifico%7CArvo%3Aregular%2Citalic%2C700%2C700italic%7CSource%20Sans%20Pro%3A200%2C200italic%2C300%2C300italic%2Cregular%2Citalic%2C600%2C600italic%2C700%2C700italic%2C900%2C900italic%7CArvo%3A400%2C400italic%2C700%2C700italic%7CSource%20Sans%20Pro%3A400%2C200%2C200italic%2C300%2C300italic%2C400italic%2C600%2C600italic%2C700%2C700italic%2C900%2C900italic%7COpen%20Sans%3A400%2C700&amp;subset=">
        <script   src="https://code.jquery.com/jquery-3.4.1.min.js"   integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="   crossorigin="anonymous"></script>
        <script src="assets/js/main.js"></script>

        <title>Judex | Klantportaal</title>

    </head>
    <body>
        <header>
            <div class="grid-container">
                <div class="grid-x grid-padding-x">
                    <div class="cell medium-3">
                        <div class="logo">
                            <a href="/"> <embed src="assets/img/logo-site-ju.png" alt="Judex - Uw recht. Uw expert." height="40"> </a>
                        </div>
                    </div>
                    <div class="cell medium-3">
                        <div class="title-text"> Klantportaal </div>
                    </div>
                    <div class="cell medium-6">
                        <div class="logout">
                            <br> masoumiprojects@gmail.com <br>
                            <a class="logout" href="/%5Eaccount/$logout/?next=/login/judex"> <b>Uitloggen</b></a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="grid-container main">
            <div class="grid-x grid-padding-x">

                <div class="medium-12 cell">

                <!-- Status selector -->
                <h3>Status:&nbsp;&nbsp;&nbsp;</h3>
                    <input id="alle" type="radio" name="gender" value="Alle"> <label for="alle">Alle</label>
                    <input id="concept" type="radio" name="gender" value="Concept"> <label for="concept">Concept</label>
                    <input id="nieuw" type="radio" name="gender" value="Nieuw"> <label for="nieuw">Nieuw</label>
                    <input id="actief" type="radio" name="gender" value="Actief"> <label for="actief">Actief</label>
                    <br>

                <!-- Keyword zoekfunctie -->
               <h3>Snel zoeken </h3>
                    <input type="text" placeholder="Begin met typen om te zoeken: Dossiernummer/naam of e-mailadres Mediator..." id="doc-filterer" autofocus/>
                    
                        <h3><span class="doc-count">0</span>&nbsp;dossiers&nbsp;(<span class="doc-type">-</span>)</h3>
                        <table id="docs-table" class="stack">
                            <thead>
                                <tr>
                                    <th>Dossier</th>
                                    <th>Mediator</th>
                                    <th>Laatste activiteit</th>
                                    <th>Vragenlijst</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody id="docs">
                            </tbody>
                        </table>
                    
                </div>
            </div>
        </div>
    </body>
</html>